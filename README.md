rgarrigue.docker
================

Install [Docker](https:/docker.com) as of 2017, i.e. `docker-ce` as in community edition, and remove the deprecated packages `docker-engine`, `docker`, pip's `docker-py` etc [![Build Status](https://travis-ci.org/rgarrigue/ansible-role-docker.svg?branch=master)](https://travis-ci.org/rgarrigue/ansible-role-docker)

Requirements
------------

Developped and tested on *Ubuntu Server 16.10 Yakkety*, and *CentOS 7*

Role Variables
--------------

The only one likely to be overrided is `user`, which default to `docker` but you might want to set to your own daily user

- apt_key_signature
- apt_key_url
- apt_repository
- yum_baseurl
- yum_gpgkey
- user
- deprecated_packages

Dependencies
------------

None

Example Playbook
----------------

    - hosts: mydockercloud
      vars:
        - user: me.myself

      roles:
        - role: rgarrigue.docker

License
-------

GPLv3

Author Information
------------------

Rémy Garrigue
